#ifndef _CTREE_H
#define _CTREE_H

#include <cstdlib>
#include <iostream>
#include <string>

// tree of characters, can be used to implement a trie

class CTree {
  friend class CTreeTest;
  char data;     // the value stored in the tree node

  CTree * kids;  // children - pointer to first child of list, maintain order & uniqueness

  CTree * sibs;  // siblings - pointer to rest of children list, maintain order & uniqueness
                 // this should always be null if the object is the root of a tree

  CTree * prev;  // pointer to parent if this is a first child, or left sibling otherwise
                 // this should always be null if the object is the root of a tree

 public:
  //Constructor makes new tree 
  //only requires char entry to create root
  //however can pass children if necessary
  //makes easier to write constuctor code
  CTree(const char ch, CTree* kids = NULL, CTree* sibs = NULL, CTree* prev = NULL);
  //copy constructor- deep copy
  CTree(const CTree& tree, bool isRoot = true);

  ~CTree();  // clear siblings to right and children and this node
  
  // siblings and children must be unique, return true if added, false otherwise
  bool addChild(const char ch);

  // add tree root for better building, root should have null prev and sibs 
  // returns false on any type of failure, including invalid root
  bool addChild(CTree *root);

  std::string toString() const; // all characters, separated by newlines, including at the end

  // find child node with data c, moving through direct kids only(first move to kid, then sibs)
  // bool is only true during first call, after that need to include false
  // to know to iterate through sibs list instead
  // helper function removeSibling
  bool removeChild(const char c);

  bool operator==(const CTree& t);

  std::ostream& operator<<(std::ostream& os);

 private:
  // these should only be called from addChild, and have the same restrictions
  // the root of a tree should never have any siblings
  // returns false on any type of failure, including invalid root
  bool addSibling(const char ch);
  bool addSibling(CTree *root);
  // helper for removeChild, should only be called from there
  bool removeSibling(const char c);

};


#endif
