#include <sstream>
#include <algorithm>
#include <cmath>
#include "FileDir.h"

FileDir::FileDir(const std::string initName, const long initSize, const bool initObjType) : name(initName), size(initSize), objType(initObjType) { 	//initializer constructor
}

FileDir::FileDir(const FileDir& fileDir){	//copy constructor
	name = fileDir.name;
	size = fileDir.size;
	objType = fileDir.objType;
}

//getters:
long FileDir::getSize() const{
	return size;
}

std::string FileDir::getName() const{
	return name;
}

bool FileDir::isFile() const{	//true if file, false if dir
	return !objType; 
}
//end getters

std::string FileDir::rename(const std::string newName) {	//change name, return old name
	std::string formerName = name;	//make temp var to hold old name for return
	name = newName;
	return formerName;
}

long FileDir::resize(const long change) {	//input change in size, not new size
	if (change >= 0) {	//if >= 0, no problems with negative size values
		size += change;
	} else {
		if (size >= std::abs(change)) {	//check if size is big enough to handle change
			size += change;
		} else {		//if not, return an error code and print to stderr
			std::cerr << "Input will make size negative" << std::endl;
		}
	}
	return size;
}

std::string FileDir::toString() const{
	std::ostringstream stringConverter;
	if (objType) {
		stringConverter << name << "/ [" << size << "kb]";
	} else {
		stringConverter << name << " [" << size << "kb]";
	}
	return stringConverter.str();
	
}

//== operator definition for FileDir

bool FileDir::operator==(const FileDir& fd) {
	//remember to ask why const doesn't work in this case
	return (fd.getName() == this->getName() && fd.isFile() == this->isFile());
}

std::ostream& operator<<(std::ostream& os, const FileDir& fd) {
	//output toString
	os << fd.toString();
	return os;
}

bool FileDir::operator<(const FileDir& fd) {
	//first see if directory or file
	//directory is less than file
	if (this->isFile() != fd.isFile()) {
		return !(this->isFile());
	} else {
		//then alphabetical ignoring caps
		//convert to lowercase then normally compare
		std::string currData = this->getName();
		std::transform(currData.begin(), currData.end(), currData.begin(), ::tolower);
		std::string otherData = fd.getName();
		std::transform(otherData.begin(), otherData.end(), otherData.begin(), ::tolower);
		
		return (currData < otherData);
	}
}
