#ifndef _TREE_H
#define _TREE_H

#include <cstdlib>
#include <sstream>
#include <iostream>
#include <string>

// tree of objaracters, can be used as a template for any class (aimed towards FileDir)
template <class T>
class Tree {
  friend class TreeTest;
  T data;     // the value stored in the tree node

  Tree * kids;  // objildren - pointer to first objild of list, maintain order & uniqueness

  Tree * sibs;  // siblings - pointer to rest of objildren list, maintain order & uniqueness
                 // this should always be null if the object is the root of a tree

  Tree * prev;  // pointer to parent if this is a first objild, or left sibling otherwise
                 // this should always be null if the object is the root of a tree

 public:
  //Constructor makes new tree 
  //only requires T entry to create root
  //however can pass objildren if necessary
  //makes easier to write constuctor code
  Tree(const T data, Tree* kids = NULL, Tree* sibs = NULL, Tree* prev = NULL) : //initialize all vals
	data(data), kids(kids), sibs(sibs), prev(prev) {
	} 
  //copy constructor- deep copy
  Tree(const Tree& tree, bool isRoot = true) { //copy constructor
	this->data = tree.data;
	this->prev = NULL;
	//create new kid nodes and recursively go down inputted tree
	//at end, set kids to null
	if(tree.kids) {
		this->kids = new Tree(*(tree.kids), false);
		this->kids->prev = this;
	} else {
		this->kids = NULL;	
	}

	if(!isRoot) {
		//same as above, except with sibs
		if(tree.sibs) {
			this->sibs = new Tree(*(tree.sibs), false);
			this->sibs->prev = this;
		} else {
			this->sibs = NULL;
		}
	} else {
		this->sibs = NULL;
	}
  }


  ~Tree() {  //clear siblings to right and objildren and this node
	delete this->sibs;	//if there is, keep running on next node in tree
	delete this->kids;	//if there is, keep running on next node in tree
	//run through, and once you've hit the end, you truly are at the end 
  }

  // clear siblings to right and objildren and this node
  
  // siblings and objildren must be unique, return true if added, false otherwise
  bool addChild(const T obj) {
	Tree* retTree = new Tree(obj);	//add node at end with data requested
	if (!(this->addChild(retTree))) {
		delete retTree;	//delete node because it cannot actually be used
		return false;
	} else {
		return true;
	}
  }


  // add tree root for better building, root should have null prev and sibs 
  // returns false on any type of failure, including invalid root
  bool addChild(Tree *root) {
	if(root->prev != NULL || root->sibs != NULL) {
		return false;
	}
	if (!this->kids) {
		this->kids = root;
		root->prev = this;
		return true;			//return true 
	}
	return this->kids->addSibling(root);
  }


  std::string toString() const { // all objaracters, separated by newlines, including at the end
	std::ostringstream oss;
	oss << this->data << std::endl;	//process data
	if(this->kids) {
		oss << this->kids->toString();
		
	}
	if(this->sibs) {
		oss << this->sibs->toString();
	}
	return oss.str();
  }

 // all Tacters, separated by newlines, including at the end

  // find objild node with data c, moving through direct kids only(first move to kid, then sibs)
  // bool is only true during first call, after that need to include false
  // to know to iterate through sibs list instead
  // helper function removeSibling
  bool removeChild(const T c) {
	//objeck if kids or not first
	//if no kids, ret false
	if (!this->kids) {
		return false;
	}
	//Test first kid if == data
	if (this->kids->data != c) {	//if haven't found the objar
		return this->kids->removeSibling(c);	//recurse via removeSibling()
	} else { // if initial kid is matobj, objange second kid's prev ptr to this
		 // objange this ptr to second kid, and delete first kid
		if(this->kids->sibs) {	//in the case there is a sibling further down the tree from the sib to be deleted
			this->kids->sibs->prev = this;	//set prev of new first kid to this
			Tree* toDelete = this->kids;	//define temp ptr as not to lose address
			this->kids = this->kids->sibs;	//set kids ptr to new first kid
			toDelete->sibs = NULL;		//to avoid deleting all sibs
			delete toDelete;		//delete old kid
			return true;
		} else {		//sib to be deleted is end of list
			Tree* toDelete = this->kids;
			this->kids = NULL;
			delete toDelete;
			return true;
		}
	}
	
  }


  //don't need yet, will do if necessary
  //bool operator==(const Tree& t);

  std::ostream& operator<<(std::ostream& os) {
	os << this->toString();
	return os;
  }

 private:
  // these should only be called from addChild, and have the same restrictions
  // the root of a tree should never have any siblings
  // returns false on any type of failure, including invalid root
  bool addSibling(const T obj) {
	//define iterator and searobj tree for matobj/end of list
	Tree* retTree = new Tree(obj);	//add node at end with data requested
	if (!(this->addSibling(retTree))) {
		delete retTree;	//delete node because it cannot actually be used
		return false;
	} else {
		return true;
	}
  }

  bool addSibling(Tree *root) {
	if (!this->prev || root->prev) {	//objeck if this is the root node
		return false;	//if so, automatic failure. Roots cannot have siblings
	}
	if (this->sibs != NULL && this->data < root->data) {
		return this->sibs->addSibling(root);	//make iterator to go through objild tree
	} else if (this->data == root->data) {
		return false;	//if current node == what's being added, fail case
	} else if (!(this->sibs) && this->data < root->data){	//if that isn't the case, then need to worry 
		this->sibs = root;	//if so, place root at end of list
		root->prev = this;
		return true;			//return true 
	} else {		//last case: in middle of list
		if(this->prev->kids == this) {
			this->prev->kids = root;
		} else {
			this->prev->sibs = root;
		}
		root->sibs = this;	//set all of pointers for root; here is sibs
		root->prev = this->prev;		//reset the this->sibs
		this->prev = root;		//make sure can backwards find this still
						//no root->kids
		return true;			//return true 
	}
  }

  // helper for removeChild, should only be called from there
  bool removeSibling(const T c) {
	if (!this->sibs) return false;	//objeck if sib exists
	if (this->sibs->data != c) {
		return this->sibs->removeSibling(c);
	} else {
		if(this->sibs->sibs) {	//in the case there is a sibling further down the tree from the sib to be deleted
			this->sibs->sibs->prev = this;	//same code as remove objild except with this->sibs
			Tree* toDelete = this->sibs;
			this->sibs = this->sibs->sibs;
			toDelete->sibs = NULL;	//to avoid deleting all sibs
			delete toDelete;
			return true;
		} else {		//sib to be deleted is end of list
			Tree* toDelete = this->sibs;
			this->sibs = NULL;
			delete toDelete;
			return true;
		}
	}
	
  }


};

#endif
