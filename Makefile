CC = g++
CXXFLAGS = -std=c++11 -pedantic -Wall -Wextra -O -g
GPROF = -pg
GCOV = -fprofile-arcs -ftest-coverage

test: fdTest ctTest ttTest
	@echo "Running tests..."
	./fdTest
	./ctTest
	./ttTest

# recompiles tests with gcov on and reports the lines not tested
testgcov:
	make clean
	make CXXFLAGS="$(CXXFLAGS) $(GCOV)" test
	gcov *.cpp
	-@grep -n "#####" *.cpp.gcov

fdTest: FileDir.cpp FileDirTest.cpp
	$(CC) $(CXXFLAGS) FileDir.cpp FileDirTest.cpp -o fdTest

ctTest: CTree.cpp CTreeTest.cpp
	$(CC) $(CXXFLAGS) CTree.cpp CTreeTest.cpp -o ctTest

# name your Tree Template file Tree.hpp
# make sure TreeTempTest.cpp includes Tree.hpp
# if all that is correct, you don't need to include it on the compile line
ttTest: FileDir.cpp TreeTest.cpp 
	$(CC) $(CXXFLAGS) FileDir.cpp  TreeTest.cpp -o ttTest

clean:
	rm -f a.out *~ *.o *.gcov *.gcda *.gcno gmon.out fdTest ctTest ttTest
