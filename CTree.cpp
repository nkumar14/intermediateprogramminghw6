#include <sstream>
#include "CTree.h"

CTree::CTree(const char ch, CTree* kids, CTree* sibs, CTree* prev) : //initialize all vals
data(ch), kids(kids), sibs(sibs), prev(prev) {
}

CTree::CTree(const CTree& tree, bool isRoot) { //copy constructor
	this->data = tree.data;
	this->prev = NULL;
	//create new kid nodes and recursively go down inputted tree
	//at end, set kids to null
	if(tree.kids) {
		this->kids = new CTree(*(tree.kids), false);
		this->kids->prev = this;
	} else {
		this->kids = NULL;	
	}

	if(!isRoot) {
		//same as above, except with sibs
		if(tree.sibs) {
			this->sibs = new CTree(*(tree.sibs), false);
			this->sibs->prev = this;
		} else {
			this->sibs = NULL;
		}
	} else {
		this->sibs = NULL;
	}
}

CTree::~CTree() {  //clear siblings to right and children and this node
	delete this->sibs;	//if there is, keep running on next node in tree
	delete this->kids;	//if there is, keep running on next node in tree
	//run through, and once you've hit the end, you truly are at the end 
}

bool CTree::addSibling(const char ch) {
	//define iterator and search tree for match/end of list
	CTree* retTree = new CTree(ch);	//add node at end with data requested
	if (!(this->addSibling(retTree))) {
		delete retTree;	//delete node because it cannot actually be used
		return false;
	} else {
		return true;
	}
}

bool CTree::addSibling(CTree *root) {
	if (!this->prev || root->prev) {	//check if this is the root node
		return false;	//if so, automatic failure. Roots cannot have siblings
	}
	if (this->sibs != NULL && this->data < root->data) {
		return this->sibs->addSibling(root);	//make iterator to go through child tree
	} else if (this->data == root->data) {
		return false;	//if current node == what's being added, fail case
	} else if (!(this->sibs) && this->data < root->data){	//if that isn't the case, then need to worry 
		this->sibs = root;	//if so, place root at end of list
		root->prev = this;
		return true;			//return true 
	} else {		//last case: in middle of list
		if(this->prev->kids == this) {
			this->prev->kids = root;
		} else {
			this->prev->sibs = root;
		}
		root->sibs = this;	//set all of pointers for root; here is sibs
		root->prev = this->prev;		//reset the this->sibs
		this->prev = root;		//make sure can backwards find this still
						//no root->kids
		return true;			//return true 
	}
}

// siblings and children must be unique, return true if added, false otherwise
bool CTree::addChild(const char ch) {
	CTree* retTree = new CTree(ch);	//add node at end with data requested
	if (!(this->addChild(retTree))) {
		delete retTree;	//delete node because it cannot actually be used
		return false;
	} else {
		return true;
	}
}

// add tree root for better building, root should have null prev and sibs 
// returns false on any type of failure, including invalid root
bool CTree::addChild(CTree *root) {
	if(root->prev != NULL || root->sibs != NULL) {
		return false;
	}
	if (!this->kids) {
		this->kids = root;
		root->prev = this;
		return true;			//return true 
	}
	return this->kids->addSibling(root);
}

std::string CTree::toString() const { // all characters, separated by newlines, including at the end
	std::ostringstream oss;
	oss << this->data << std::endl;	//process data
	if(this->kids) {
		oss << this->kids->toString();
		
	}
	if(this->sibs) {
		oss << this->sibs->toString();
	}
	return oss.str();
}

bool CTree::removeChild(const char c) {
	//check if kids or not first
	//if no kids, ret false
	if (!this->kids) {
		return false;
	}
	//Test first kid if == data
	if (this->kids->data != c) {	//if haven't found the char
		return this->kids->removeSibling(c);	//recurse via removeSibling()
	} else { // if initial kid is match, change second kid's prev ptr to this
		 // change this ptr to second kid, and delete first kid
		if(this->kids->sibs) {	//in the case there is a sibling further down the tree from the sib to be deleted
			this->kids->sibs->prev = this;	//set prev of new first kid to this
			CTree* toDelete = this->kids;	//define temp ptr as not to lose address
			this->kids = this->kids->sibs;	//set kids ptr to new first kid
			toDelete->sibs = NULL;		//to avoid deleting all sibs
			delete toDelete;		//delete old kid
			return true;
		} else {		//sib to be deleted is end of list
			CTree* toDelete = this->kids;
			this->kids = NULL;
			delete toDelete;
			return true;
		}
	}
	
}

bool CTree::removeSibling(const char c) {
	if (!this->sibs) return false;	//check if sib exists
	if (this->sibs->data != c) {
		return this->sibs->removeSibling(c);
	} else {
		if(this->sibs->sibs) {	//in the case there is a sibling further down the tree from the sib to be deleted
			this->sibs->sibs->prev = this;	//same code as remove child except with this->sibs
			CTree* toDelete = this->sibs;
			this->sibs = this->sibs->sibs;
			toDelete->sibs = NULL;	//to avoid deleting all sibs
			delete toDelete;
			return true;
		} else {		//sib to be deleted is end of list
			CTree* toDelete = this->sibs;
			this->sibs = NULL;
			delete toDelete;
			return true;
		}
	}
	
}

/* function was there for sake of test use when running into bugs
 * but didn't end up using so commented out
 * equal function partly worked, but needed to be fixed as to not evaluate
 * siblings of root nodes that are subtrees
 * however, not necessary for assignment, so can be commented otu
std::ostream& CTree::operator<<(std::ostream& os) {
	os << this->toString();
	return os;
}

bool CTree::operator==(const CTree& t) {
	// first check initial data is true
	if (!(t.data == this->data)) return false;
	
	//check both have a kids or both don't have a a kid	
	//if different cases, return false
	if (!(t.kids) && this->kids) return false;
	//next check if there are kids
	if (t.kids) {
		//if there are compare the values of the kid nodes(so it will recurse through)
		//if they are not the same, call false
		if (!(t.kids == this->kids)) return false;
	}
	
	//repeat process with sibs
	if (!(t.sibs) && this->sibs) return false;
	//next check if there are sibs
	if (t.sibs) {
		//if there are compare the values of the kid nodes(so it will recurse through)
		//if they are not the same, call false
		if (!(t.sibs == this->sibs)) return false;
	}

	//prev is irrelevant because does not matter what is before, matters what is contained by both trees
	//prevs within the tree have already been evaluated

	//if survives all comparison cases, must be true
	return true;
	
}
 */
