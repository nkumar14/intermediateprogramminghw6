#ifndef _FILEDIR_H
#define _FILEDIR_H
#include <string>
#include <iostream>

/*
 * Class FileDir (Object is either file or directory)
 * 
 * Requirements:
 * -All data must be private
 * -const for parameters wherever possible
 * -public functions:
 *	<> = return, () = params
 *	Constructor passed string name, long(default 4), and bool(true = dir, default false = file)
 *	Constructor copy constructor
 *	getters for <string> name and <long> size
 *	<bool> isFile- returns true if file, false if dir
 *	<string> rename (string)- takes in string of new name, returns old name (and replaces old name)
 *	<long> resize (long)- if param > 0, add to prev size. if param < 0, add to prev size only if param + prevsize >= 0 (to avoid error). No change otherwise, and return new size.
 *	<string> toString- return a string containing the item name and size (format "size" kb) with a space in between. If directory, include '/' after name.
 */

class FileDir {
	friend std::ostream& operator<<(std::ostream& os, const FileDir& fd);
	private:
		std::string name;	//name
		long size;	//size of file/dir in kb
		bool objType;	//true if dir or false if file
	public:
		FileDir(const std::string initName, const long initSize = 4, const bool initObjType = false);	//initializer constructor
		FileDir(const FileDir& fileDir);	//copy constructor
		//getters:
		long getSize() const;
		std::string getName() const;
		bool isFile() const;	//true if file, false if dir
		std::string rename(const std::string newName);	//change name, return old name
		long resize(const long change);	//input change in size, not new size
		std::string toString() const;		
		bool operator==(const FileDir& fd);
		bool operator<(const FileDir& fd);
};

std::ostream& operator<<(std::ostream& os, const FileDir& fd);

#endif
