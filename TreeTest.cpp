#include "Tree.hpp"
#include "FileDir.h"
#include <string>
#include <vector>
#include <cassert>
#include <iostream>

using std::cout;
using std::endl;
using std::vector;

class TreeTest {
public:
/* toString and destructor tested throughout (run valgrind to test
 * destructor): "A toString() function that creates and returns a
 * string of all the elements in the nodes of the tree, separated by
 * newline characters. The ordering of the nodes must be determined by
 *
 * a depth-first pre-order traversal from the root node" "A destructor
 * that clears all the children of the current node and all the
 * siblings to the right of the current node, freeing that dynamically
 * allocated memory. (Hint: use recursion!)"
 */


/* Constructor:
 * "A constructor that is passed the character to store in the root
 * node of a new tree. 
 */
    static void constructorTest() {
        // build a few trees with constructor
	// first 2 primitive types (char and int)
        Tree<char> t1('A');
        assert(t1.toString() == "A\n");
        Tree<int> t2(1);
        assert(t2.toString() == "1\n");

	// then with std::string
        Tree<std::string> t3("A");
        assert(t3.toString() == "A\n");

	// then with FileDir
	FileDir fd("fdirtest", 4, true);
        Tree<FileDir> t4(fd);
        assert(t4.toString() == "fdirtest/ [4kb]\n");

	FileDir fd2("filedtest", 4, false);
        Tree<FileDir> t5(fd2);
        assert(t5.toString() == "filedtest [4kb]\n");
    }


/* Test adding children and siblings:
 * "An 'addChild' function, which is passed the character data to be
 * stored in the child. This function returns false if it fails
 * (because the data is already a child of this tree node) and true if
 * it succeeds. Remember that it must keep the children ordered."
 *
 * "An 'addSibling' function, which is passed the data to be stored in
 * a new sibling node. This function returns false if it fails
 * (because the data is already a sibling t * o the right of this tree
 * node) and true if it succeeds. Remember that siblings must always
 * be ordered (the hard part of the function, worth 5 points). (This
 * function is really just a helper function for addChild, but keeping
 * it public will let us test it easily.)"
 */

    template <class T>
    static void addsTest(T inputArr[], std::string inputStr[]) {
        // A
        Tree<T>* t1Ptr = new Tree<T>(inputArr[0]);
        Tree<T>& t1 = *t1Ptr; // avoids a bunch of *'s below
        assert(t1.toString() == inputStr[0]);

        // A
        // |
        // b
        assert(t1.addChild(inputArr[1]));
        assert(t1.toString() == inputStr[1]);
        // can't add again
        assert(!t1.addChild(inputArr[1]));
        assert(t1.toString() == inputStr[1]);

        // A
        // |
        // b - c
        assert(t1.addChild(inputArr[2]));
        assert(t1.toString() == inputStr[2]);
        // can't add again
        assert(!t1.addChild(inputArr[2]));
        assert(t1.toString() == inputStr[2]);

        // A
        // |
        // B - b - c
        assert(t1.addChild(inputArr[3]));
        // inputArr[3] comes before inputArr[1]
        assert(t1.toString() == inputStr[3]);
        // can't add repeats
        assert(!t1.addChild(inputArr[3]));
        assert(!t1.addChild(inputArr[1]));
        assert(!t1.addChild(inputArr[2]));
        assert(t1.toString() == inputStr[3]);

        // can't add inputArr[0] as sibling of inputArr[0]
        assert(!t1.addSibling(inputArr[0]));
        assert(t1.toString() == inputStr[3]);

        // make sure that we can't add siblings to the root
        assert(!t1.addSibling(inputArr[6]));
        assert(t1.toString() == inputStr[3]);

        // Adding in an already built subTree<T>
        // First build another tree
        // R
        Tree<T>* t2Ptr = new Tree<T>(inputArr[4]);
        Tree<T>& t2 = *t2Ptr;
        assert(t2.toString() == inputStr[4]);
        
        // R
        // |
        // C
        assert(t2.addChild(inputArr[6]));
        assert(t2.toString() == inputStr[5]);


        // R
        // |
        // C - d
        assert(t2.addChild(inputArr[7]));
        assert(t2.toString() == inputStr[6]);
        // can't repeat
        assert(!t2.addChild(inputArr[7]));
        assert(t2.toString() == inputStr[6]);

        // R
        // |
        // B - C - d
        assert(t2.addChild(inputArr[3]));
        assert(t2.toString() == inputStr[7]);
        // can't repeat
        assert(!t2.addChild(inputArr[3]));
        assert(t2.toString() == inputStr[7]);

        // Add t1 in as a child
        // R
        // |
        // A - B - C - d
        // |
        // B - b - c

        // t1 is as before
        assert(t1.toString() == inputStr[3]);
        // add t1 to t2
        assert(t2.addChild(&t1));
        // t1 should now have siblings
        assert(t1.toString() == inputStr[8]);
        // t2 should be updated
        assert(t2.toString() == inputStr[9]);

        // R
        // |
        // @ - A - B - C - d
        //     |
        //     B - b - c
        assert(t2.addChild(inputArr[8]));
        assert(t2.toString() == inputStr[10]);
        // shouldn't be able to add duplicate children
        assert(!t2.addChild(inputArr[8]));
        assert(!t2.addChild(inputArr[0]));
        assert(!t2.addChild(inputArr[3]));
        assert(!t2.addChild(inputArr[6]));
        assert(!t2.addChild(inputArr[7]));

        // R
        // |
        // @ - A - B - C - D - d
        //     |
        //     B - b - c
        assert(t2.addChild(inputArr[9]));
        assert(t2.toString() == inputStr[11]);

        // R
        // |
        // @ - A - B - C - D - d - e
        //     |
        //     B - b - c
        assert(t2.addChild(inputArr[10]));
        assert(t2.toString() == inputStr[12]);

        delete t2Ptr;
        
    }

    // adds a single child
    template <class T>
    static void addSimpleChildTest(T inputArr[], std::string stringArr[]) {
	//Keep tests from CTree merely to test with primitive type
        // A
        Tree<T>* t1 = new Tree<T>(inputArr[0]);
        assert(t1->toString() == stringArr[0]);

        // A
        Tree<T>* t2 = new Tree<T>(inputArr[1]);
        assert(t2->toString() == stringArr[1]);

        // A
        // |
        // A
        assert(t2->addChild(t1));
        assert(t2->toString() == stringArr[1] + stringArr[0]);

        delete t2;
    }

};



int main(void) {
	cout << "Testing Tree" << endl;
	TreeTest::constructorTest();
	//for addSimpleChild
	//need to make inputArr(objs being inputted)
	//and stringArr(expected strings)
	//for each data type

	//char:
	char charArr[2] = {'A', 'B'};
	std::string charStr[2] = {"A\n", "B\n"};
	TreeTest::addSimpleChildTest<char>(charArr, charStr);

	//int:
	int intArr[2] = {1, 2};
	std::string intStr[2] = {"1\n", "2\n"};
	TreeTest::addSimpleChildTest<int>(intArr, intStr);

	//std::string:
	std::string strArr[2] = {"A", "B"};
	std::string strStr[2] = {"A\n", "B\n"};
	TreeTest::addSimpleChildTest<std::string>(strArr, strStr);

	//FileDir:
	FileDir dir("fdirtest", 4, true);
	FileDir file("filedtest", 4, false);
	FileDir fileDirArr[2] = {dir, file};
	std::string fileDirStr[2] = {"fdirtest/ [4kb]\n", "filedtest [4kb]\n"};
	TreeTest::addSimpleChildTest<FileDir>(fileDirArr, fileDirStr);

	//for addsTest
	//need array of strings that assert will match up with individual statements
	//and array of objs to feed in
	//that way can template function to easily use for multiple data types
	//9 of each

        // R
        // |
        // @ - A - B - C -D - d - e
        //     |
        //     B - b - c

	// 5
        // |
        // 9 - 1 - 7 - 6 - 10 - 8 - 11
        //     |
        //     4 - 2 - 3

	//char:
	char charArr2[11] = {'A', 'b', 'c', 'B', 'R', 'B', 'C', 'd', '@', 'D', 'e'};
	std::string charStr2[13] = {"A\n", "A\nb\n", "A\nb\nc\n", "A\nB\nb\nc\n", "R\n", "R\nC\n", "R\nC\nd\n", "R\nB\nC\nd\n", "A\nB\nb\nc\nB\nC\nd\n", "R\nA\nB\nb\nc\nB\nC\nd\n", "R\n@\nA\nB\nb\nc\nB\nC\nd\n", "R\n@\nA\nB\nb\nc\nB\nC\nD\nd\n", "R\n@\nA\nB\nb\nc\nB\nC\nD\nd\ne\n"};
	TreeTest::addsTest<char>(charArr2, charStr2);

	//int:
	int intArr2[11] = {1,6,7,2,5,2,3,8,0,4,9};
	std::string intStr2[13] = {"1\n", "1\n6\n", "1\n6\n7\n", "1\n2\n6\n7\n", "5\n", "5\n3\n", "5\n3\n8\n", "5\n2\n3\n8\n", "1\n2\n6\n7\n2\n3\n8\n", "5\n1\n2\n6\n7\n2\n3\n8\n", "5\n0\n1\n2\n6\n7\n2\n3\n8\n", "5\n0\n1\n2\n6\n7\n2\n3\n4\n8\n", "5\n0\n1\n2\n6\n7\n2\n3\n4\n8\n9\n"};
	TreeTest::addsTest<int>(intArr2, intStr2);

	//std::string:
	std::string stringArr2[11] = {"A", "b", "c", "B", "R", "B", "C", "d", "@", "D", "e"};
	std::string stringStr2[13] = {
		"A\n",
		"A\nb\n",
		"A\nb\nc\n",
		"A\nB\nb\nc\n",
		"R\n",
		"R\nC\n",
		"R\nC\nd\n",
		"R\nB\nC\nd\n",
		"A\nB\nb\nc\nB\nC\nd\n",
		"R\nA\nB\nb\nc\nB\nC\nd\n",
		"R\n@\nA\nB\nb\nc\nB\nC\nd\n",
		"R\n@\nA\nB\nb\nc\nB\nC\nD\nd\n",
		"R\n@\nA\nB\nb\nc\nB\nC\nD\nd\ne\n"};
	TreeTest::addsTest<std::string>(stringArr2, stringStr2);

	//FileDir:
	//All capital letters can be dirs(including @)
	//@ : dir@, 3, dir
	FileDir fd0("dir@", 3, true);
	//A : dirA, 4, dir
	FileDir fdA("dirA", 4, true);
	//B : dirB, 2, dir
	FileDir fdB("dirB", 2, true);
	//C : dirC, 1, dir
	FileDir fdC("dirC", 1, true);
	//D : dirD, 4, dir
	FileDir fdD("dirD", 4, true);
	//R : dirR, 5, dir
	FileDir fdR("dirR", 5, true);
	//a : filea, 6, dir
	FileDir fda("filea", 6, false);
	//b : fileb, 7, dir
	FileDir fdb("fileb", 7, false);
	//c : filec, 8, dir
	FileDir fdc("filec", 8, false);
	//d : filed, 9, dir
	FileDir fdd("filed", 9, false);
	//e : filee, 19, dir
	FileDir fde("filee", 19, false);

	FileDir FileDirArr2[11] = {fdA, fdb, fdc, fdB, fdR, fdB, fdC, fdd, fd0, fdD, fde};
	std::string FileDirStr2[13] = {
		fdA.toString() + "\n",
		fdA.toString() + "\n" + fdb.toString() + "\n",
		fdA.toString() + "\n" + fdb.toString() + "\n" + fdc.toString() + "\n",
		fdA.toString() + "\n" + fdB.toString() + "\n" + fdb.toString() + "\n" + fdc.toString() + "\n",
		fdR.toString() + "\n",
		fdR.toString() + "\n" + fdC.toString() + "\n",
		fdR.toString() + "\n" + fdC.toString() + "\n" + fdd.toString() + "\n",
		fdR.toString() + "\n" + fdB.toString() + "\n" + fdC.toString() + "\n" + fdd.toString() + "\n",
		fdA.toString() + "\n" + fdB.toString() + "\n" + fdb.toString() + "\n" + fdc.toString() + "\n" + fdB.toString() + "\n" + fdC.toString() + "\n" + fdd.toString() + "\n",
		fdR.toString() + "\n" + fdA.toString() + "\n" + fdB.toString() + "\n" + fdb.toString() + "\n" + fdc.toString() + "\n" + fdB.toString() + "\n" + fdC.toString() + "\n" + fdd.toString() + "\n",
		fdR.toString() + "\n" + fd0.toString() + "\n" + fdA.toString() + "\n" + fdB.toString() + "\n" + fdb.toString() + "\n" + fdc.toString() + "\n" + fdB.toString() + "\n" + fdC.toString() + "\n" + fdd.toString() + "\n",
		fdR.toString() + "\n" + fd0.toString() + "\n" + fdA.toString() + "\n" + fdB.toString() + "\n" + fdb.toString() + "\n" + fdc.toString() + "\n" + fdB.toString() + "\n" + fdC.toString() + "\n"+ fdD.toString() + "\n" + fdd.toString() + "\n",
		fdR.toString() + "\n" + fd0.toString() + "\n" + fdA.toString() + "\n" + fdB.toString() + "\n" + fdb.toString() + "\n" + fdc.toString() + "\n" + fdB.toString() + "\n" + fdC.toString() + "\n"+ fdD.toString() + "\n" + fdd.toString() + "\n" + fde.toString() + "\n"};
	TreeTest::addsTest<FileDir>(FileDirArr2, FileDirStr2);
	
    cout << "Tree tests passed" << endl;
}
