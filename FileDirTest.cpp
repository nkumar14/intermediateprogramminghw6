#include "FileDir.h"
#include <string>
#include <cassert>
#include <sstream>
#include <iostream>

using std::string;
using std::endl;
using std::cout;
using std::istringstream;
using std::ostringstream;


// Tests constructors and accessors:
//
// - A constructor which is passed a string name, a long size and a
// boolean value which is true if the item is a directory and false if
// it's a file. Include default parameter values of 4 for the size and
// false for boolean.
//
// - A copy constructor.
//
// - Functions that return the size and the name of the item, long
// getSize() and string getName() respectively.
//
// - A function called "isFile" that returns true if the object is a
// file, and false if it is a directory.

void basicsTest() {
    // basic use of constructor
    FileDir d1("testDir", 4, false);
    assert(d1.getName() == "testDir");
    assert(d1.getSize() == 4);
    assert(d1.isFile());

    // do defaults work as expected?
    FileDir d2("testDir");
    assert(d2.getName() == "testDir");
    assert(d2.getSize() == 4);
    assert(d2.isFile());

    // equality check
    assert(d1 == d2);

    // try with a big num and a directory
    long bigNum = 2 << 18;
    bool isDir = true;
    FileDir d3("testDir", bigNum, isDir);
    assert(d3.getName() == "testDir");
    assert(d3.getSize() == bigNum);
    assert(!d3.isFile());

    // same name but still shouldn't be equal
    assert(!(d3 == d2));

    // make sure that copy constructor works
    FileDir d4 = d2;
    assert(d4 == d2);
    FileDir d5 = d3;
    assert(d5 == d3);

    // test with a different name
    FileDir d6("differentname");
    assert(d6.getName() == "differentname");
    assert(d6.getSize() == 4);
    assert(d6.isFile());
    assert(!(d6 == d2));
    
    // we don't expect special handling of white space and punctuation
    // in name, right?
    string messyName = "29872 &*(2 messy name !/\n\n\n/\n//";
    FileDir d7(messyName);
    assert(d7.getName() == messyName);

}

// Tests mutators (rename and resize)
//
// - A mutator called "rename" that is passed a string to use in order
// to rename the item. This function should return the former name.
// 
// - A mutator called "resize" that updates the item size according to a
// number passed as a parameter. If the parameter is >0, it's value
// should be added to the current size. If it is negative, it should
// be subtracted from the size, but only if the result will be >=
// 0. Make no changes if the parameter is invalid. In either case
// return the (new) current size.

void mutatorsTest() {
    // basic use of constructor
    FileDir d1("testDir", 5);
    assert(d1.getName() == "testDir");
    assert(d1.getSize() == 5);
    assert(d1.isFile());

    assert(d1.rename("differentName") == "testDir");
    assert(d1.getName() == "differentName");
    assert(d1.getSize() == 5);
    assert(d1.isFile());

    // empty name
    assert(d1.rename("") == "differentName");
    assert(d1.getName() == "");

    // subtract to exactly 0
    assert(d1.resize(-5) == 0);
    assert(d1.getSize() == 0);

    // add from 0
    assert(d1.resize(10) == 10);
    assert(d1.getSize() == 10);

    // add 0
    assert(d1.resize(0) == 10);
    assert(d1.getSize() == 10);

    // add from positive number
    assert(d1.resize(5) == 15);
    assert(d1.getSize() == 15);

    // subtract not to zero
    assert(d1.resize(-4) == 11);
    assert(d1.getSize() == 11);

    // try to subtract "past" 0
    assert(d1.resize(-12) == 11);
    assert(d1.getSize() == 11);

    // subtract to 0
    assert(d1.resize(-11) == 0);
    assert(d1.getSize() == 0);

    // try to subtract from 0
    assert(d1.resize(-10000) == 0);
    assert(d1.getSize() == 0);

}
/* Uncomment if you write these for fun...  ---------- cut here ------

// Test read
//
// - A function to read an item name and size from a parameter input
// stream. If the last character of the name string is a slash ('/'),
// then the item is a directory; otherwise it is a file. However, do
// not store the '/' as part of the name.

void readTest() {
    // input stream with some interesting whitespace in there
    istringstream input("file1\n78 \tdir1/ 100 dir2/ 0 \n\nfile2 101\n ");
    FileDir d("testDir", 3, true);
    d.read(input);
    assert(d.getName() == "file1");
    assert(d.getSize() == 78);
    assert(d.isFile());

    d.read(input);
    assert(d.getName() == "dir1");
    assert(d.getSize() == 100);
    assert(!d.isFile());

    d.read(input);
    assert(d.getName() == "dir2");
    assert(d.getSize() == 0);
    assert(!d.isFile());

    d.read(input);
    assert(d.getName() == "file2");
    assert(d.getSize() == 101);
    assert(d.isFile());

    // make sure that nothing changes if I'm at the end of the stream
    d.read(input);
    assert(d.getName() == "file2");
    assert(d.getSize() == 101);
    assert(d.isFile());
    
}

void writeTest() {
    // regular file
    FileDir d("testFile", 50, false);
    ostringstream out;
    d.write(out);
    out << " ";
    d.write(out);
    string s = out.str();
    assert(s == "testFile [50kb] testFile [50kb]");

    // directory
    FileDir d2("testDir", 51, true);
    ostringstream out2;
    d2.write(out2);
    out2 << " ";
    d2.write(out2);
    string s2 = out2.str();
    assert(s2 == "testDir/ [51kb] testDir/ [51kb]");

}

--------------- end cut ----------- */

void toStringTest() {
    // regular file
    FileDir d("testFile", 50, false);
    assert(d.toString() == "testFile [50kb]");

    // directory
    FileDir d2("testDir", 51, true);
    assert(d2.toString() == "testDir/ [51kb]");

}

//Test ==, <, and << to make sure they are ready
//for the next part of the project
void operatorsTest() {
	//==
	//fd1 and fd2
	//name: fdireqs
	//size: 4
	//ftype: file (false)
	FileDir fd1 ("fdireqs", 4, false); 
	FileDir fd2 ("fdireqs", 4, false); 
	assert(fd1 == fd2);

	//<
	//2 cases:
	//dir vs file (dir > files)
	//name1 < name2

	//Case 1:

	//fd1
	//name: fdireqs
	//size: 4
	//ftype: file (false)
	
	//fd3
	//name: fdireqs
	//size: 4
	//ftype: dir (true)
	FileDir fd3 ("fdireqs", 4, true); 
	assert(fd3 < fd1);


	//Case 2:
	
	//fd1
	//name: fdireqs
	//size: 4
	//ftype: file (false)

	//fd4
	//name: feq
	//size: 4
	//ftype: file (false)
	FileDir fd4 ("feq", 4, false); 
	assert(fd1 < fd4);
	assert(!(fd1 < fd2));

	//Case 2 checking uppercase doesn't matter

	//fd4
	//name: feq
	//size: 4
	//ftype: file (false)
	
	//fd5
	//name: FEQ 
	//size: 4
	//ftype: file (false)
	FileDir fd5 ("FEQ", 4, false); 
	
	//fd6
	//name: Feq 
	//size: 4
	//ftype: file (false)
	FileDir fd6 ("Feq", 4, false); 

	//fd7
	//name: FDq 
	//size: 4
	//ftype: file (false)
	FileDir fd7 ("FDq", 4, false); 

	assert(!(fd4 < fd5));
	assert(!(fd4 < fd6));
	assert(!(fd5 < fd6));
	assert(fd7 < fd4);

	//<< operator

	//fd1
	//name: fdireqs
	//size: 4
	//ftype: file (false)

	//fd3
	//name: fdireqs
	//size: 4
	//ftype: dir (true)

	//fd4
	//name: feq
	//size: 4
	//ftype: file (false)

	std::ostringstream oss;
	oss << fd1;
	assert(oss.str() == "fdireqs [4kb]");
	
	oss.str("");
	oss << fd3;
	assert(oss.str() == "fdireqs/ [4kb]");

	oss.str("");
	oss << fd4;
	assert(oss.str() == "feq [4kb]");
}

int main(void) {
    cout << "Running FileDir tests..." << endl;
    basicsTest();
    mutatorsTest();
    //    readTest();    // not required
    //    writeTest();   // not required
    toStringTest();
    operatorsTest();
    cout << "FileDir tests passed." << endl;

}
